//function moondata2(lon,lat,squareGrid,userInput3,userInput4){
//function moondata2(lat,lon,squareGrid,userInput3,userInput4){


function moondata2(lat,lon,squareGrid,userInput3,best){
//function moondata2(lat,lon,squareGrid,userInput3,bestTime){
  
  //date_input = String(userInput3.value) + ' ' + String(userInput4.value);
  date_input = String(userInput3.value) + ' ' +best[1];
console.log(date_input);

  var sun_data = SunCalc.getPosition(new Date(date_input),lat,lon);
  var moon_data = SunCalc.getMoonPosition(new Date(date_input),lat,lon);

  

//test
  // date_input = '1984/01/03 15:21';
  // var sun_data = SunCalc.getPosition(new Date(date_input),15.6,35.6);
  // var moon_data = SunCalc.getMoonPosition(new Date(date_input),15.6,35.6);



  function degrees(radians) {
    return radians * (180 / Math.PI);
  }

  function calculatecos(deg){
    var rad = (Math.PI/180)*deg;
    return Math.cos(rad);
  }

  var sun_azi = degrees(sun_data.azimuth);
  var moon_azi = degrees(moon_data.azimuth);

    let DAZ= (sun_azi) - (moon_azi);
    console.log('DAZ=' + DAZ);

    let ARCV = Math.abs((degrees(sun_data.altitude))-(degrees(moon_data.altitude)));
    console.log('ARCV=' + ARCV);

    let cosARCV = calculatecos(ARCV);
    let cosDAZ = calculatecos(DAZ);

    let cosARCL=0;
    //let ARCL=0;

    if (ARCV>=22 || DAZ>=22){
      var ARCL = Math.acos(cosARCV*cosDAZ);
      console.log('ARCL.1=' + ARCL);
    }else{
      var ARCL = Math.sqrt(Math.pow(ARCV,2)+Math.pow(DAZ,2));
      console.log('ARCL.2=' + ARCL);
    }
    
    let p = degrees(Math.asin(6378.14/moon_data.distance))*60;

    let SD = 0.27245*p;

    let SD1= SD*(1+(Math.sin(degrees(moon_data.altitude))*Math.sin(p)));

    let W= (SD1*(1-(Math.cos(ARCL))))/3600;
   
    let q = (ARCV - (11.8371 - (6.3226*W) + (0.7319*Math.pow(W,2)) - (0.1018*Math.pow(W,3))))/10;
    console.log('q='+q);

    var greenIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var redIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var blueIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var yellowIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var blackIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-black.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var orangeIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });

    function style2(feature) {
        return {
            fillColor: feature,
            weight: 1,
            opacity: 0.1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.3
        };
    }
  
    if (q > 0.216){
      L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
    }else if (q > -0.014){
      L.geoJSON(squareGrid,{style:style2('orange')}).addTo(this.map);
    }else if (q > -0.160){
      L.geoJSON(squareGrid,{style:style2('green')}).addTo(this.map);
    }else if (q > -0.232){
      L.geoJSON(squareGrid,{style:style2('blue')}).addTo(this.map);
    }else if (q > -0.293){
      L.geoJSON(squareGrid,{style:style2('red')}).addTo(this.map);
    } else if ( q <= -0.293){
      L.geoJSON(squareGrid,{style:style2('black')}).addTo(this.map);
    }

//     console.log('best[0] --->>>>'+best[0])
//   if (best[0] == true){
//     L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
//   }else if(best[2] == true){
//     L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
//   }else {
//     if (q > 0.216){
//       L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
//     }else if (q > -0.014){
//       L.geoJSON(squareGrid,{style:style2('orange')}).addTo(this.map);
//     }else if (q > -0.160){
//       L.geoJSON(squareGrid,{style:style2('green')}).addTo(this.map);
//     }else if (q > -0.232){
//       L.geoJSON(squareGrid,{style:style2('blue')}).addTo(this.map);
//     }else if (q > -0.293){
//       L.geoJSON(squareGrid,{style:style2('red')}).addTo(this.map);
//     } else if ( q <= -0.293){
//       L.geoJSON(squareGrid,{style:style2('black')}).addTo(this.map);
//     }
// }



   

    function style(feature) {
      return {
          fillColor: getColor(q),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.7
      };
    }
}
