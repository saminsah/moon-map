
  //function bestTime(lat, lon, userInput3, userInput4){
  function bestTime(lat, lon, userInput3){

    //date_input = String(userInput3.value) + ' ' + String(userInput4.value);
    date_input = String(userInput3.value);
    
    var times = SunCalc.getTimes(new Date(date_input), lat,lon);
    
    min = times.sunset.getMinutes();
    min_0 = (min < 10 ? "0" : "") + min;

    var sunset = times.sunset.getHours() + ':' + min_0;
    console.log('sunset= '+sunset);
    
    var moonSetFlag_down = false;
    var moonSetFlag_set = false;

    var moontimes = SunCalc.getMoonTimes(new Date(date_input), lat,lon,true);
    // console.log('lat - >' + lat + ' lon - > '+lon);
    // console.log('moontimes - >' + moontimes);
    // console.log('moontimes rise - >' + moontimes.rise);
    // console.log('moontimes up - >' + moontimes.alwaysUp);
    // console.log('moontimes down - >' + moontimes.alwaysDown);
    // console.log('moontimes set - >' + moontimes.set);


    // if(moontimes.alwaysDown==true){
    // //var moonset = times.sunset.getHours() + ':' + times.sunset.getMinutes();
    // var moonset = "22:00";
    // moonSetFlag_down = true;
    // console.log('moon always down');
    // }else if(moontimes.set){
    //   var moonset = moontimes.set.getHours() + ':' + moontimes.set.getMinutes();
    //   //var moonset = "22:00";
    //   console.log('moonset= '+moonset);
    // }
    // else{
    //   moonSetFlag_set = true;
    //   var moonset = "22:00";
    //   //var moonset = moontimes.rise.getHours() + ':' + moontimes.rise.getMinutes();
    //   console.log('moonset= '+moonset);

    // }


    if(moontimes.set){
        var moonset = moontimes.set.getHours() + ':' + moontimes.set.getMinutes();
        //var moonset = "22:00";
        console.log('moonset= '+moonset);
      }
      else{
        moonSetFlag_set = true;
        var moonset = "22:00";
        //var moonset = moontimes.rise.getHours() + ':' + moontimes.rise.getMinutes();
        console.log('moonset= '+moonset);
  
      }

      function timeToDecimal(t) {
        t = t.split(':');
        return parseFloat(parseInt(t[0], 10) + parseInt(t[1], 10)/60).toFixed(2);
      }  
    
      function minTommss(minutes){
        var sign = minutes < 0 ? "-" : "";
        var min = Math.floor(Math.abs(minutes));
        var sec = Math.floor((Math.abs(minutes) * 60) % 60);
        return sign + (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
      }
    
    var bestTime = minTommss((5*timeToDecimal(sunset) + 4*timeToDecimal(moonset))/9);
    console.log('bestTime= '+bestTime);

    return [moonSetFlag_down, bestTime,moonSetFlag_set];
    }
    